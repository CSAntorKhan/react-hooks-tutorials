import React from 'react';
import logo from './logo.svg';
import './App.css';
import ClassMouse from './components/ClassMouse'
import ClassCounter from './components/ClassCounter'
import ClassCounterOne from './components/ClassCounterOne'
import ClassCounterTwo from './components/ClassCounterTwo'
import HookMouse from './components/HookMouse'
import HookCounter from './components/HookCounter'
import HookCounterOne from './components/HookCounterOne'
import HookCounterTwo from './components/HookCounterTwo'
import HookCounterThree from './components/HookCounterThree'
import HookCounterFour from './components/HookCounterFour';
import MouseContainer from './components/MouseContainer';
function App() {
  return (
    <div className="App">
      {/* <ClassCounter/>
      <HookCounter/>
      <HookCounterTwo/>
      <ClassCounterTwo/>
      <HookCounterThree/> */}
      {/* <div><HookCounterFour/></div> */}
      {/* <div><ClassCounterOne/></div> */}
      {/* <div><HookCounterOne/></div> */}
      {/* <ClassMouse/> */}
      {/* <HookMouse/> */}
      <MouseContainer/>

    </div>
  );
}

export default App;
 