import React, {useState} from 'react'

function HookCounterFour() {
    const [items,setItems] = useState([{id:1, value: 1},{id:2, value: 2}]);
    const addItem = () => {
        console.log('clicked');
        setItems([...items, {
            id: items.length,
            value: Math.floor(Math.random() * 10) + 1
        }])
    }

    return (
        <div>
            <button onClick={addItem}>Add a number</button>
            <ul>
            {
                items.map( item => (
                    <li key={item.id}>{item.value}</li>
                ))
            }
            </ul>

        </div>
    )
}

export default HookCounterFour
