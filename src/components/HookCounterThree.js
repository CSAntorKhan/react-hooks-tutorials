import React,{useState} from 'react'

function HookCounterThree() {
    const [name, setName] =  useState({firstname:'', lastname: ''});

    return (
        <div>
            <form>
                <input type={'text'} onChange={ e => {setName({... name,firstname: e.target.value})}}/>
                <input type={'text'} onChange={ e => {setName({... name,lastname: e.target.value})}}/>
                 <h2>Your firstname is - {name.firstname}</h2>
                 <h2>Your lastname is - {name.lastname}</h2>
            </form>
        </div>
    )
}

export default HookCounterThree
